local M = {}

local defaults = {
	create_keymaps = true,
	keymap = "<c-s>",
	stopinsert = true,
	display_path = "relative", -- "relative|absolute|parent|file|filenoext"
}

M.options = defaults

M.setup = function(user_options)
	M.options = vim.tbl_extend("force", defaults, user_options or {})
end

return M
