local M = {}
local actions = require("verboseupdate.actions")

M.set_keymaps = function()
	local config = require("verboseupdate.config")
	if config.options.create_keymaps == false then
		return
	end
	vim.keymap.set({ "n", "x", "i" }, config.options.keymap, actions.verboseupdate)
end

return M
