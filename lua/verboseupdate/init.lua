local M = {}

M.setup = function(user_options)
	require("verboseupdate.config").setup(user_options)
	require("verboseupdate.keymaps").set_keymaps()
end

return M
