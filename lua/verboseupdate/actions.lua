local M = {}
local cmd = vim.cmd
local fn = vim.fn
local bo = vim.bo
local config = require("verboseupdate.config")

local function get_display_path()
	local type = config.options.display_path
	return type == "relative" and fn.expand("%")
		or type == "absolute" and fn.expand("%:p")
		or "parent" and fn.expand("%:h")
		or "file" and fn.expand("%:t")
		or "filenoext" and fn.expand("%:t:r")
end

M.verboseupdate = function()
	if bo.buftype == "nofile" then
		return
	end

	if bo.ro == true then
		vim.notify(string.format("Can't update '%s' (readonly)", get_display_path()), "error")
	else
		local ok, write_err = pcall(function()
			cmd("silent write")
			vim.notify("Updated " .. fn.expand("%"))
		end)

		if not ok then
			vim.notify(string(write_err), "error")
		end
	end
	if config.options.stopinsert then
		vim.cmd("stopinsert")
	end
end

return M
